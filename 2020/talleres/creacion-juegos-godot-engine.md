---
layout: 2020/post
section: proposals
category: workshops
title: Creación de juegos 2D/3D con Godot Engine
---

[Godot Engine](https://godotengine.org/) es un motor de creación de juegos 2D/3D Open Source que compite en la actualidad con los motores líderes en el mercado: Unity y Unreal. Con Godot se están creando juegos profesionales en todos los ámbitos del mercado y para todas las plataformas. Su desarrollo [durante más de 10 años](https://godotengine.org/article/retrospective-and-future) ha alcanzado una madurez que le está dando una gran popularidad en la actualidad.

Godot proporciona metodologías, un entorno integrado completo, un lenguaje de scripting que simplifica la programación, APIs, integración con motor de física Bullet, documentación y una comunidad amplia (más de 1000 contribuidores en GitHub) para ayudar en la creación de juegos 2D/3D profesionales, que funcionan en Linux, Windows, Android, iOS o Web entre otros.

## Objetivos a cubrir en el taller

Durante el taller el objetivo es conocer el proyecto, el editor donde se crean los juegos, su lenguaje de scripting de programación (GDScript) y el desarrollo de un juego 2D basado en el tutorial oficial de Godot pero con evoluciones que ha terminado en el juego Open Source "Numbers Attack": <https://github.com/acs/numbers_attack>. El final del taller será la creación de versiones para Linux, Web y Android del juego.

Si quedara también, se darán las primeras pinceladas de como se crean juegos en 3D, que es una evolución sobre lo aprendido en 2D.

## Público objetivo

Cualquier desarrollador con interés en el ámbito de los videojuegos y de la creación de experiencias multimedia interactivas.

## Ponente(s)

**Alvaro del Castillo San Felix**. En la pasada edición de esLibre presentó el taller de big data.

### Contacto(s)

-   **Alvaro del Castillo San Felix**: alvaro.delcastillo at gmail.com

## Prerrequisitos para los asistentes

Será necesario tener un equipo (Linux preferentemente) en el que instalar Godot.

## Prerrequisitos para la organización

Ninguo específico para el taller.

## Tiempo

El taller da pie a tener una duración flexible en función de cuanto se quiera cubrir y con que necesidad. Con 2 horas debería de ser suficiente para una introducción.

## Día

Preferible el sábado 19 de Septiembre.

## Comentarios

Ninguno.

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/).
-   [x]  Al menos una persona entre los que proponen el taller estará presente el día programado para el mismo.
-   [x]  Acepto coordinarme con la organización de esLibre.
