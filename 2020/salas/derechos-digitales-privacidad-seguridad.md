---
layout: 2020/post
section: proposals
category: devrooms
title: Derechos Digitales, Privacidad en Internet y Seguridad Informática
---

En un momento en el que las grandes compañías tecnológicas se frotan las manos pensando en todo el beneficio que podrán sacar de nuestros datos o el peligro que puede entrañar la tecnología más inocente del mundo si se le da usos con objetivos cuestionables (o peor, inconscientes), es más necesario que nunca disponer del conocimiento sobre cómo funcionan las tecnologías que está tan integradas en nuestro día a día que nos rodean de forma casi inapreciable. Hoy más que nunca debemos ser conscientes de la necesidad de proteger nuestra privacidad y nuestra soberanía digital: tenemos que saber **QUÉ** tenemos a nuestra disposición y **CÓMO** podemos actuar; y esto, sin duda alguna, es algo que viene de la mano del **Software Libre**.

Más información en nuestra página web: [https://interferencias.tech/eslibre2020/](https://interferencias.tech/eslibre2020/)

## Comunidad o grupo que lo propone

Desde **[Interferencias](https://interferencias.tech)** siempre nos gusta colaborar en iniciativas que nos parecen interesantes, y es por eso que tanto el [año pasado](https://eslib.re/2019/) como [este año](https://eslib.re/2020/) estamos colaborando en la organización de **esLibre**; no podemos más que brindar todo nuestro apoyo a quien quiera reivindicar la importancia del conocimiento y la difusión del las _tecnologías libres y cultura abierta_.  

Anualmente realizamos un evento propio para crear espacios donde las personas puedan expresar libremente sus inquietudes en materia de derechos digitales y seguridad informática, las **[Jornadas de Anonimato, Seguridad y Privacidad (JASYP)](https://interferencias.tech/jasyp)**, pero que este año no hemos podido realizar debido a la situación actual. Todos los años planteamos las Jornadas como una celebración donde compartir experiencias y conocimiento con la espontaneidad del trato en persona, es por eso que este año decidimos que no tendría sentido celebrarlas de forma online, se perdería su significado: _no se trata de ser otro congreso, se trata de ser una experiencia aprovechando todo lo que nos brinda Granada_.

Por todo esto que comentamos, finalmente hemos decidido presentar una **propuesta de sala** dentro del programa de **esLibre** para seguir llegando a tanta gente como sea posible.

### Contactos

-   **Paula de la Hoz**: @terceranexus6 Telegram/Gitlab
-   **Germán Martínez**: germaaan at interferencias dot tech

## Público objetivo

Todo el mundo.

## Tiempo

En principio, un día.

## Día

Indiferente.

## Formato

Un programa compuestos por charlas de entre 10 y 30 minutos con el siguiente contenido:

-   12:30-12:40 <span style="font-size:20px; text-decoration: underline; ">**Presentación**</span><br><br>

-   12:40-13:10 <span style="font-size:20px; text-decoration: underline; ">**Pablo Castro: _Herramientas para mantener tu privacidad en Internet_**</span>

    En pleno 2020, muchas grandes empresas tecnológicas basan su modelo de negocio en vender nuestra vida privada, nuestra forma de ser, qué vamos a ser en el futuro, llegando incluso a manipularnos.

    En esta charla veremos rápidamente los peligros a los que se enfrenta la sociedad tecnológica actual y qué herramientas podemos tener e instalar de forma sencilla para intentar reducir la información que generamos a diario y así luchar por preservar nuestra privacidad.
    <br><br>

-   13:10-13:45 <span style="font-size:20px; text-decoration: underline; ">**Jorge Cuadrado: _Self Sovereign Identity, identidad bajo el control de las personas_**</span>

    [Self Sovereign Identity (SSID)](https://es.wikipedia.org/wiki/Auto-Identidad_Soberana) es un sistema de identificación que ha surgido recientemente y que, conceptualmente, cambia significativamente respecto a modelos más tradicionales de identificación.

    Mediante este esquema, las personas adquieren la responsabilidad de gestionar cómo y en qué cantidad se usan sus datos personales. Para ello, permite crear “tarjetas de identidad” digitales que pueden ser generales para cualquier servicio o específicas para cada caso. Pero... ¿qué ventajas y peligros hay detrás de SSID?
    <br><br>

-   16:00-16:25 <span style="font-size:20px; text-decoration: underline; ">**Emma Lopez: _¿Existe vacuna para los sesgos en las inteligencias artificiales?_**</span>

    Los humanos tenemos sesgos y parece que las máquinas _inteligentes_ también, pero, ¿se trata esto de una _feature_ o de un _bug_?

    En esta charla veremos qué son las llamadas inteligencias artificiales y qué son los sesgos, por qué es tal fácil que permeen en el software, cuál es su impacto y qué podemos hacer para detectarlos y mitigarlos.
    <br><br>

-   16:25-16:40 <span style="font-size:20px; text-decoration: underline; ">**Javier Cantón: _Fact-checking y fake news: caja de herramientas ciudadana para la verificación_**</span>

    Saturados de información y desconfianza, resulta cada vez más complicado discernir la información cierta de la falsa. La proliferación de las denominadas fake news está reconfigurando la esfera pública y los cauces de formación de la opinión pública, lo que supone un reto para nuestras democracias, que se fundamentan en el acceso a una información veraz para sus ciudadanos.

    En este taller aprenderás diferentes métodos y herramientas libres para verificar la procedencia de una noticia y luchar contra las fake news y los bulos.
    <br><br>

-   16:40-17:15 <span style="font-size:20px; text-decoration: underline; ">**Lorena Sánchez: _Social Justice Code Repositories_**</span>

    ¿Garantiza el software libre que el usuario sea libre? ¿Son necesarios y suficientes los principios del FSM para garantizar la justicia? ¿Cuál es el rol de los repositorios de código en la promoción de la justicia en el software? Estas son algunas de las preguntas que intentaremos desgranar en la charla con el propósito principal de analizar bajo qué condiciones el Free Software Movement funciona para alcanzar la justicia en la producción, uso y consumo de software. Así, la base de la charla será retar el funcionamiento de las tesis de Richard Stallman como teoría, poniendo en valor el rol de los repositorios de código abierto y buscando los puntos débiles que deben ser mejorados.

    Si bien la charla presenta un punto de vista teórico, pretende ponerle sentido al FSM a través de diferentes teorías de la justicia preguntándose cómo el FSM puede ser clave en la libertad del usuario sin quedarse en papel mojado.
    <br><br>

-   17:30-17:55 <span style="font-size:20px; text-decoration: underline; ">**Paula de la Hoz: _Software Libre para las personas_**</span>

    Desde su origen, el Software Libre está ligado a la comunidad, a ser partícipe de proyectos con el ánimo de que lleguen a otras manos, las que sean, y sigan mejorando.

    En la actualidad, desde asociaciones hasta grupos de vecinos se enfrentan a diario a un mundo ya protagonizado por la tecnología, y es aquí donde el software libre debería ser una herramienta esencial que defienda los intereses de las personas sin que ello suponga ceder seguridad y privacidad.

    En esta charla pretendo presentar las posibles soluciones libres para las necesidades de asociaciones y otras comunidades, entre ellas [BarrioCheck](https://gitlab.com/terceranexus6/barriocheck), un proyecto para alertas de barrios basado en OSM que desarrollé hace poco y que pretende dar comienzo en Madrid.

    Durante la charla pretenden cubrirse los siguientes temas: comunicación segura y libre, hardening básico e introducción a sysadmin de comunidades, instrucciones y recursos para nivel de usuario para miembros no técnicos, BarrioCheck, hardware y software libre para centros/bibliotecas/radios sociales; entre otros.
    <br><br>

-   17:55-18:20 <span style="font-size:20px; text-decoration: underline; ">**Sofía Prósper: _Hacktivismo no es nombre de señora_**</span>

    Viviendo el año más extraño de nuestras vidas, este atípico 2020 nos deja con nuestros derechos digitales afectados y con unos precedentes en solucionismo tecnológico que nadie hubiera imaginado si una pandemia no los hubiera instaurado velozmente.

    El capitalismo de la vigilancia es sólo un síntoma de un sistema económico y político funcionando como se espera. Por eso es el momento idóneo para movernos, para investigar, para estar informadas y no parar de defender nuestras libertades antes de que la tecnología ‘nos venga a salvar’.

    En esta charla nos gustaría, a parte de hacer una pequeña introducción de cuál es el estado actual de las cosas, mostrar proyectos e iniciativas que no se han rendido al ‘yo no tengo nada que esconder’ y siguen al pie del cañón llegando cada día a más gente.
    <br><br>

-   18:20-18:45 <span style="font-size:20px; text-decoration: underline; ">**Luis Fajardo: _¿Hacia un feudalismo de los datos? ¿Derecho o tecnología? ¿Cómo defendernos?_**</span>

    La Administración está llena de malas prácticas, el mercado está dirigido a primar la corporocracia, que está sustituyendo a la democracia.

    El Fediverso, las redes federadas, y los servicios libres (que pueden ser instalados "on premise"), pero no parecen suficientes. Realmente tenemos normas que intentan limitar el avance de la corporocracia, fundamentalmente el RGPD. Lamentablemente no se está usando, y debe lanzarse un aviso a navegantes.

    -   Cómo nos enganchan: dispositivos que no obedecen a sus dueños, redes sociales que polarizan, manipulación informativa e información individualizada...

    -   Cómo defenderse: elección de los servicios con unos parámetros técnicos (arranque libre, open source, interoperable, estándar y federado...) y jurídicos (que permite exigir aquellos criterios técnicos, básicamente, RGPD).
    <br><br>

## Comentarios

Ninguno.

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x]  Al menos una persona entre los que proponen la devroom estará presente el día agendado para la _devroom_.
-   [x]  Acepto coordinarme con la organización de esLibre.
-   [x]  Entiendo que si no hay un programa terminado para la fecha que establezca la organización, la _devroom_ podría retirarse.
