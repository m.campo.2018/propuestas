---
layout: 2020/post
section: proposals
category: talks
title: TÍTULO DE LA PROPUESTA
---

Pequeña introducción y motivación de la misma.

## Formato de la propuesta

Indicar uno de estos:

-   [ ]  Charla (25 minutos)
-   [ ]  Charla relámpago (10 minutos)

## Descripción

Descripción de un par de párrafos sobre de qué va la charla.

## Público objetivo

¿A quién va dirigida?

## Ponente(s)

¿Quién o quienes van a dar la charla? ¿Qué hacen? ¿Qué charlas han
dado antes?

### Contacto(s)

-   Nombre: contacto

Para "Nombre", utliza el nombre completo. Para "contacto", utiliza una dirección de correo (formato "usuario @ dominio"), o el nombre de usuario en GitLab (formato "usuario @ GitLab"). En cualquier caso, ten en cuenta que estas direcciones se usarán para entrar en contacto contigo, así que mejor si las consultas frecuentemente ;-)

## Comentarios

Cualquier otro comentario relevante.

## Condiciones

-   [ ]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [ ]  Al menos una persona entre los que la proponen estará presente el día programado para la charla.
